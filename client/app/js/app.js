'use strict';

/* App Module */
var teamChatApp = angular.module('teamChatApp', [
  'ngRoute',
  'teamChatControllers'
]);

teamChatApp.config([
    '$routeProvider',
    function($routeProvider){
        $routeProvider.
        when('/home', {
            templateUrl:'partials/home.html',
            controller: 'HomePageCtrl'
        }).
        when('/login', {
            templateUrl:'partials/login.html',
            controller: 'LoginPageCtrl'
        }).
        when('/chat', {
            templateUrl:'partials/group_chat.html',
            controller: 'GroupChatCtrl'
        }).
        otherwise({
            redirectTo: '/home'
        });
    }
]);
