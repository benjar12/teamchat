'use strict';

/* Controllers */

//i know globals bad..
var siderbarbtns = [
        {name: 'home', location: '#/'},
        {name: 'login', location: '#/login'},
        {name: 'register', location: '#/login'},
        {name: 'about', location: '#/login'}
];
//this would be more dynamiclly configured
var serverBase = 'http://localhost:3000/';


var teamChatControllers = angular.module('teamChatControllers', []);

teamChatControllers.controller('HomePageCtrl', function($scope, $http){
    $scope.title = "My Super awesome chat app.";
    
    $scope.siderbarbtns = siderbarbtns;

});


teamChatControllers.controller('LoginPageCtrl', ['$scope', '$routeParams', '$http',
function($scope, $routeParams, $http){
    
    $scope.title = "One step away from my awesome chat app.";
    
    $scope.siderbarbtns = siderbarbtns;
    
    $scope.loginalerts = [];
    $scope.registeralerts = [];
    
    
    $scope.login = function(){
        
        
        
    };
    
    $scope.register = function(){
        
        console.log('binding works');
        
        var user = $scope.rusername,
            pass = $scope.rpassword;

        //hash the password TO-DO convert to sha1
        pass = CryptoJS.MD5(pass).toString();
        
        console.log("here", user, pass);
        
        //validate
        //should add some min password validation and check that the user does not exist
        if(!user || !pass){
            //error show error
            $scope.registeralerts.push({ type: 'danger', msg: 'Oh snap! Missing username or Password' });
        }else{
            //post
            var url = serverBase + 'user';
            var data = {username:user, passwordHash:pass};
            $http.post(url, data).success(function(data){
                console.log(data);
                window.location.href = "#/chat";
           }).error(function(data){
               $scope.registeralerts.push({ type: 'danger', msg: 'Oh snap! server error try again later' });
           });
        }
        
    };
    
}]);

teamChatControllers.controller('GroupChatCtrl', function($scope, $http){
    $scope.title = "Chat With Your Team.";
    
    //will be populated by the mongoose model
    $scope.siderbarbtns = [{name: '#main', location: '#'},
        {name: '#coolstuff', location: '#'}];
    
    //{align:right|left, name:username, body:..., minutesago:...}
    $scope.messages = [];
    $scope.messages.push({
        align: 'left',
        name: 'benjarman',
        body: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare dolor, quis ullamcorper ligula sodales.',
        //this should really be a date object and compared to now.
        minutesago: '15'
    });
    
    //add polling or websockets logic here.

});
