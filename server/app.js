var logger = require('./lib/logger'),
    config = require('./lib/config')(),
    express = require('express'),
    app = express(),
    bodyParser = require('body-parser');
    //deal with oath later
    //oauthserver = require('node-oauth2-server');

app.use(bodyParser());

//parse various different custom JSON types as JSON
app.use(bodyParser.json({ type: 'application/*+json' }));

//parse some custom thing into a Buffer
app.use(bodyParser.raw({ type: 'application/vnd.custom-type' }));

//parse an HTML body into a string
app.use(bodyParser.text({ type: 'text/html' }));

//this will mess with newrelic but works
app.all('*', function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//heartbeat url
app.get('/heartbeat', function(req, res){
    res.send({succes:true});
});

// create application/json parser
var jsonParser = bodyParser.json();

//register route files. Could be moved to a router to clean up the app file
var user = require('./app/user');
app.get('/user', user.get);
app.post('/user', jsonParser, user.post);
//for now we will assume that we never need to delete a user
app.delete('/user', jsonParser, user.delete);



var server = app.listen(config.getProperty('server_port'), function () {

    var host = server.address().address;
    var port = server.address().port;

    logger.info('App listening at http://%s:%s', host, port);

});