var crypto = require('crypto');
var model = require('../dao/users')();
var logger = require('../lib/logger');

//if i had time i would create a route prototype so i 
//dont have to use the copy paste design pattern so much. ;)
module.exports = {
    get : function(req, res){
        
        //normally should return the user that was 
        //validated by the oauth middleware 
        //but im going to make it take a query param for now
        var username = req.query.username,
            passwordHash =  req.query.password;
        
        var r = {success:true, status:200, error:""};
        
        //should do more validation and sanitation
        if(!username || !passwordHash){
            r.status = 400;
            r.error = 'Missing params';
            res.status(r.status).send(r);
        
        }else{
            
            //I really could move this hashing business to the dao/model but eh
            var uid = crypto.createHash('md5').update(username).digest('hex');
            
            model.findOne({_id:uid, passwordHash:passwordHash}, cb);
            
            function cb(err, data){
                
                if(err){
                    
                    logger.error(err);
                    //this should be controlled by the config and a debug flag
                    r.error = err;
                    r.success = false;
                    status = 500;
                
                }
                if(data){
                    
                    r.data = data;
                    
                }else{
                    
                    r.error = "No user found!";
                    r.success = false;
                    r.status = 404;
                    
                }
                
                res.status(r.status).send(r);
                
                
            }
        
        }
        
    },
    post : function(req, res){
        
        var data = req.body;
        //rather than null i could use the field as a mapping value
        //or just use the map funciton.
        var requiredvalues = {username:null, passwordHash:null};
        var r = {success:true, status:200, error:""};
        
        for(var key in requiredvalues) { requiredvalues[key] = (data[key]) ? data[key] : missingRequired(key); }
        
        function missingRequired(value){
            
            r.error += "Missing "+ value+" ";
            r.success = false;
            r.status = 400;
            return null;
            
        }
        
        //stuff goes here for optional params
        
        if(!r.success){
            res.status(r.status).send(r);
        }else{
            //password will be sha1 hashed on the frontend
            requiredvalues._id = crypto.createHash('md5').update(requiredvalues.username).digest('hex');
            var user = new model(requiredvalues);
            
            user.save(dbCallback);
            
            function dbCallback(err){
                if(err){
                    logger.error( err );
                    r.success = false;
                    r.status = 500;
                    //should have a debugging option in config
                    r.error = err;
                }
                
                res.status(r.status).send(r);
            }
        }
        
    },
    
    delete: function(req, res){
        
        res.status(400).send({error:'method not supported'});
    }
};
