var logger = require('./logger');
//TO-DO Configure from config.js
var host = 'localhost',
    db = 'demo';
module.exports = require('mongoose').connect('mongodb://'+host+'/'+db, cb);

function cb(err){
    if(err){logger.error(err);}
    else{logger.info("Successfully Connected to: %s/%s", host, db);} 
}
