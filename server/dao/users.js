module.exports = function(){
    var mongoose = require('../lib/mongoose'),
        Schema = mongoose.Schema;
        
    var UsersSchema = new Schema({
        _id          : String, // hash of the username
        passwordHash : String, //should be a hashed on the client side and stored here
        created      : {type:Date, default: Date.now}
    });
    
    var model = mongoose.model("Users", UsersSchema);
    
    return model;
};